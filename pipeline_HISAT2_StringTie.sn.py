import json
import time
import os
from glob import glob

# Some file systems (like ours), update slowly. This can cause snamake errors
# for job that finish quickly as the out files can appear newer than the input files. 
shell.prefix(" sleep $[ ( $RANDOM % 2 )  + 1 ] ; ")
# Get data from the config file. 
configfile: "config.json"

VERSION       = config["VERSION"]
BASE_PATH     = config["BASE_PATH"]
INDEX_BASE    = config["INDEX_BASE"]
TAXID         = config["TAXID"]
SOFTWARE_PATH = config["SOFTWARE_PATH"]


FASTQ_FOLDER = "FASTQ/"
COMBINED_FASTQ_FOLDER = "COMBINED_FASTQ/"
FASTQ_1 = COMBINED_FASTQ_FOLDER + "{sample}_1.fastq"
FASTQ_2 = COMBINED_FASTQ_FOLDER + "{sample}_2.fastq"

HISAT2_FOLDER = "HISAT2/"
HISAT2_SAM    = HISAT2_FOLDER + "{sample}.sam"
HISAT2_STATS  = HISAT2_FOLDER + "{sample}.stats"
HISAT2_BAM    = HISAT2_FOLDER + "{sample}.bam"
SINGLE_UNALIGNED = HISAT2_FOLDER + "{sample}.single_unaligned"
DOUBLE_UNALIGNED = HISAT2_FOLDER + "{sample}.double_unaligned"

STRINGTIE_FOLDER = "STRINGTIE/"
STRINGTIE_GTF    = STRINGTIE_FOLDER + "{sample}.gtf"
STRINGTIE_GENES  = STRINGTIE_FOLDER + "{sample}.genes.tsv"

MERGED_GTF_FOLDER = "MERGED_GTF/"
MERGED_CONDITONS  = MERGED_GTF_FOLDER + "%s.merge_list.txt" % TAXID
MERGED_GTF_FILE   = MERGED_GTF_FOLDER + "merged.%s.gtf" % TAXID

#CUFFQUANT_FOLDER = "CUFFQUANT/"
CUFFQUANT_FOLDER = "CUFFQUANT/%s/{sample}/" % TAXID
CUFFQUANT_CXB    = CUFFQUANT_FOLDER + "abundances.cxb" 

CUFFNORM_FOLDER                = "CUFFNORM/"
CUFFNORM_CONDITONS_FILE        = CUFFNORM_FOLDER + "%s.cuffnorm_conditions.txt" % TAXID
CUFFNORM_OUT_DIR               = CUFFNORM_FOLDER + "%s/" % TAXID
CUFFNORM_GENES_FPKM_TABLE      = CUFFNORM_OUT_DIR + "genes.fpkm_table"
CUFFNORM_TRANSCRIPT_FPKM_TABLE = CUFFNORM_OUT_DIR + "isoforms.fpkm_table"

DATASETS = []
for line in open(config["samples"]):
    DATASETS.append(line.strip())


rule all:
    input:
        #expand("FASTQs/{sample}_1.fastq", sample=DATASETS),
        # 
        #expand(HISAT2_SAM,   sample=DATASETS),
        #expand(HISAT2_STATS, sample=DATASETS),
        #expand(HISAT2_BAM,   sample=DATASETS),
        #
        #expand(STRINGTIE_GTF,   sample=DATASETS),
        #expand(STRINGTIE_GENES, sample=DATASETS),
        #
        #expand(MERGED_CONDITONS, tax_id=TAX_IDS_TO_RUN),
        #expand(MERGED_GTF_FILE, tax_id=TAX_IDS_TO_RUN),
        #
        expand(MERGED_CONDITONS),
        expand(MERGED_GTF_FILE),
        # 
        expand(CUFFQUANT_FOLDER, sample=DATASETS),
        expand(CUFFQUANT_CXB, sample=DATASETS),
        # 
        #expand(CUFFNORM_CONDITONS_FILE),
        #expand(CUFFNORM_OUT_DIR),
        #expand(CUFFNORM_GENES_FPKM_TABLE),
        #expand(CUFFNORM_TRANSCRIPT_FPKM_TABLE),




rule HISAT2:
    input:
        fastq_1  = FASTQ_1,
        fastq_2  = FASTQ_2
    output:
        sam = temp(HISAT2_SAM),
        stats = HISAT2_STATS,
    run:
        shell("mkdir -p %s ;" % HISAT2_FOLDER)
        tax_id = TAXID  
        # https://github.com/griffithlab/rnaseq_tutorial/blob/master/manuscript/supplementary_tables/supplementary_table_5.md 
        index_str = BASE_PATH+INDEX_BASE+"%s/%s.%s.tran" % (tax_id, tax_id, VERSION,) 
        shell(SOFTWARE_PATH + """hisat2 --dta-cufflinks --mm -p 8 %s \
              -1 {input.fastq_1}   \
              -2 {input.fastq_2}   \
              --rna-strandness RF  \
              --un-gz      {output.sam}.single_strand_unaligned.gz \
              --un-conc-gz {output.sam}.double_strand_unaligned.gz \
              -S {output.sam} > {output.stats} 2>&1 """ % index_str)


# Caution: This will have problems with older versions of samtools
# samtools view -bS SRR2146358.sam | samtools sort -o SRR2146358.bam
rule Sam_to_Bam:
    input:
        sam = HISAT2_SAM
    output:
        bam = HISAT2_BAM
    shadow: "shallow"
    run:
        # For the newer version of samtools.
        shell(SOFTWARE_PATH + """samtools sort -@ 8 -o {output.bam} {input.sam} """)
        

rule StringTie:
    input:
        bam      = HISAT2_BAM, 
    output:
        genes = STRINGTIE_GENES,
        gtf   = STRINGTIE_GTF
    run:
        shell("mkdir -p %s ;" %  STRINGTIE_FOLDER)
        tax_id = TAXID
        gtf_file = BASE_PATH+INDEX_BASE+"%s/%s.%s.gtf" % (tax_id, tax_id, VERSION,)

        shell(SOFTWARE_PATH + """stringtie -p 8 -G %s {input.bam} -A {output.genes} -o {output.gtf} """ % (gtf_file,) )


rule generate_merge_samples_file:
    input:
        expand(STRINGTIE_GTF, sample=DATASETS)
    output:
        merge_list = MERGED_CONDITONS
    run:
        shell("mkdir -p %s ;" %  MERGED_GTF_FOLDER)

        with open(output.merge_list, 'w') as out:
            print(*input, sep="\n", file=out)


rule stringtie_merge_gtfs:
    input:
        merge_conditions = MERGED_CONDITONS
    output:
        merged_gtf = MERGED_GTF_FILE
    run:
        shell("mkdir -p %s ;" %  MERGED_GTF_FOLDER)
 
        tax_id = TAXID
        gtf_file = BASE_PATH+INDEX_BASE+"/%s/%s.%s.gtf" % (tax_id, tax_id, VERSION,)
        shell("""stringtie --merge -p 8 -G %s {input.merge_conditions} -o {output.merged_gtf} """ % (gtf_file,) )


rule cuffquant:
    input:
        merged_gtf = MERGED_GTF_FILE,
        bam = HISAT2_BAM
    output:
        out_dir = CUFFQUANT_FOLDER,
        cxb = CUFFQUANT_CXB
    run:
        shell("mkdir -p {output.out_dir} ;")
        shell("""cuffquant -o {output.out_dir} {input.merged_gtf} {input.bam} """ )


rule generate_cuffnorm_samples_file:
    input:
        expand(CUFFQUANT_CXB, sample=DATASETS)
    output:
        cuffnorm_list = CUFFNORM_CONDITONS_FILE
    run:
        shell("mkdir -p %s ;" %  CUFFNORM_FOLDER)
        with open(output.cuffnorm_list, 'w') as out:
            print(*input, sep=" ", file=out)


rule cuffnorm:
    input:
        merged_gtf = MERGED_GTF_FILE,
        conditions = CUFFNORM_CONDITONS_FILE
    output:
        out_dir = CUFFNORM_OUT_DIR,
    run:
        shell("mkdir -p {output.out_dir} ;")

        conditions_text = open(input.conditions).read()

        shell("""cuffnorm --output-dir {output.out_dir} {input.merged_gtf} %s """ % conditions_text)

